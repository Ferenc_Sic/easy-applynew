import { ref, computed } from 'vue';

export default function ValidatorHelper() {

    type ValidationResult = {
        status: boolean,
        errors: {
            total: 0,
            messages: {
                [key:string]: string
            }
        }
    };

    const ValidatorData = ref({
        validating: {
            what: Object
        },
    });

    const ValidatorChecker = () => {

        return {
            email: (email: string) => {
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
            },

            minlen( string : string, args: Array<Number> ) {
                return string.length >= args[0];
            },

            required(string: string) {
                return this.minlen(string, [1]);
            },

            on(value: boolean) {
                return value;
            },

            off(value: boolean) {
                return !value;
            },

            equals(value: string, field : string) {

                if (typeof (ValidatorData.value.validating.what as any)[field] !== 'undefined') {

                    return value === (ValidatorData.value.validating.what as any)[field];

                } else { return false; }

            },

            trash(value: any) {
                return true;
            }

        };

    }

    const Validator = (rules: {
        [key: string]: Array<string>;
    }) => {

        const Checker : {
            [key: string]: Function
        } = ValidatorChecker();

        return {

            Validate: (object: {
                [key: string]: any;
            }) => {
                //@ts-ignore
                ValidatorData.value.validating.what = object;

                let result : ValidationResult = {
                    status: true,
                    errors: {
                        total: 0,
                        messages: {},
                    }
                };

                for (let field in rules) {

                    if (typeof object[field] !== 'undefined') {

                        for (let r = 0; r < rules[field].length; r++) {

                            let rule = rules[field][r];

                                let args = (rule.split(':')).slice(1);

                                if (!Checker[rule.replace(/:[0-9a-z]+/i, '')](object[field], args)) {

                                    if (typeof result.errors.messages[field] === 'undefined') {

                                        result.status = false;

                                        result.errors.total++;

                                        result.errors.messages[field] = String(field) + '_' + String(rule);

                                    } // else {  }

                                } // else {  }

                            }

                    } else { throw("Field " + field + " is not defined on the object.") }
                }

                return result;

            }

        };

    };

    const FormObjectValidator = (FormObject: {
        loaders: { [key:string]: boolean };
        data: { [key: string]: string };
        rules: { [key: string]: Array<string>; };
    }) => {
        return Validator(FormObject.rules).Validate(FormObject.data);
    };

    return { ValidatorData, ValidatorChecker, Validator, FormObjectValidator }

}