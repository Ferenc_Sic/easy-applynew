import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function ShopKit() {

	const ShopKitData = ref({});

	const getProducts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/products`,
			{ params: payload, headers }
		);
	}

	const getProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const createProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const updateProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const deleteProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const getGetProducts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/products`,
			{ params: payload, headers }
		);
	}

	const getPrices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/prices`,
			{ params: payload, headers }
		);
	}

	const getPrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const createPrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const updatePrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const deletePrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const getOrders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/orders`,
			{ params: payload, headers }
		);
	}

	const getOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const createOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const putOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const updateOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/local/order`,
			{ params: payload, headers }
		);
	}

	const deleteOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const getStripeProducts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/products`,
			{ params: payload, headers }
		);
	}

	const getStripeProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/product`,
			{ params: payload, headers }
		);
	}

	const stripePaymentIntent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payment/intent`,
			{ params: payload, headers }
		);
	}

	const getStripeOrders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/orders`,
			{ params: payload, headers }
		);
	}

	const putStripeOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/order`,
			{ params: payload, headers }
		);
	}

	const getStripeOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/order`,
			{ params: payload, headers }
		);
	}

	const putStripeCustomer = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customer`,
			{ params: payload, headers }
		);
	}

	const getStripeCustomer = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customer`,
			{ params: payload, headers }
		);
	}

	const transactStock = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product/transact/stock`,
			{ params: payload, headers }
		);
	}

	const sendOrderEmails = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/order/send/emails`,
			{ params: payload, headers }
		);
	}

	const getGetProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const postCreateProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const putUpdateProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const deleteRemoveProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product`,
			{ params: payload, headers }
		);
	}

	const productTransactStock = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product/transact/stock`,
			{ params: payload, headers }
		);
	}

	const getGetPrices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/prices`,
			{ params: payload, headers }
		);
	}

	const getGetPrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const postCreatePrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const putUpdatePrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const deleteRemovePrice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/price`,
			{ params: payload, headers }
		);
	}

	const getGetOrders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/orders`,
			{ params: payload, headers }
		);
	}

	const getGetOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const postCreateOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const putUpdateOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const deleteRemoveOrder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/order`,
			{ params: payload, headers }
		);
	}

	const postPaymentOrderDeliveryIntent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payment/order/delivery/intent`,
			{ params: payload, headers }
		);
	}

	const postPaymentOrderPickupIntent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payment/order/pickup/intent`,
			{ params: payload, headers }
		);
	}

	const postPaymentIntent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payment/intent`,
			{ params: payload, headers }
		);
	}

	const postStripeOrderSendEmails = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/order/send/emails`,
			{ params: payload, headers }
		);
	}

	const getStripeRetrieveCustomer = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customer`,
			{ params: payload, headers }
		);
	}

	const postStripeCreateCustomer = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customer`,
			{ params: payload, headers }
		);
	}

	const postUpdateProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product/update`,
			{ params: payload, headers }
		);
	}

	const postRemoveProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/product/remove`,
			{ params: payload, headers }
		);
	}

	const getStripeOrderModalInformation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/orders/{order}/modal/information`,
			{ params: payload, headers }
		);
	}

	const getStripeCustomers = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customers`,
			{ params: payload, headers }
		);
	}

	const getStripeCustomerModalInformation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/customer/{customer}/modal/information`,
			{ params: payload, headers }
		);
	}

	const getStripePayments = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payments`,
			{ params: payload, headers }
		);
	}

	const getStripePayouts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/payouts`,
			{ params: payload, headers }
		);
	}

	const getStripeBalances = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/balances`,
			{ params: payload, headers }
		);
	}

	const getStripeDocuments = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/documents`,
			{ params: payload, headers }
		);
	}

	const getStripeBalanceTransactions = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/dashboard/balance/transactions`,
			{ params: payload, headers }
		);
	}

	const deleteStripeProductRemove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['shop.kit']}/api/stripe/product/{product}/remove`,
			{ params: payload, headers }
		);
	}

	return { ShopKitData, getProducts, getProduct, createProduct, updateProduct, deleteProduct, getGetProducts, getPrices, getPrice, createPrice, updatePrice, deletePrice, getOrders, getOrder, createOrder, putOrder, updateOrder, deleteOrder, getStripeProducts, getStripeProduct, stripePaymentIntent, getStripeOrders, putStripeOrder, getStripeOrder, putStripeCustomer, getStripeCustomer, transactStock, sendOrderEmails, getGetProduct, postCreateProduct, putUpdateProduct, deleteRemoveProduct, productTransactStock, getGetPrices, getGetPrice, postCreatePrice, putUpdatePrice, deleteRemovePrice, getGetOrders, getGetOrder, postCreateOrder, putUpdateOrder, deleteRemoveOrder, postPaymentOrderDeliveryIntent, postPaymentOrderPickupIntent, postPaymentIntent, postStripeOrderSendEmails, getStripeRetrieveCustomer, postStripeCreateCustomer, postUpdateProduct, postRemoveProduct, getStripeOrderModalInformation, getStripeCustomers, getStripeCustomerModalInformation, getStripePayments, getStripePayouts, getStripeBalances, getStripeDocuments, getStripeBalanceTransactions, deleteStripeProductRemove };
}