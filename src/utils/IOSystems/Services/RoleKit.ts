import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function RoleKit() {

	const RoleKitData = ref({});

	const postUpdateRole = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['role.kit']}/api/roles/update/role`,
			{ params: payload, headers }
		);
	}

	const postCreateRole = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['role.kit']}/api/roles/create/role`,
			{ params: payload, headers }
		);
	}

	return { RoleKitData, postUpdateRole, postCreateRole };
}