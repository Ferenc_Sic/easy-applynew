import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function TODOKit() {

	const TODOKitData = ref({});

	const todayItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/today`,
			{ params: payload, headers }
		);
	}

	const scheduledItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/scheduled`,
			{ params: payload, headers }
		);
	}

	const flaggedItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/flagged`,
			{ params: payload, headers }
		);
	}

	const allItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/all`,
			{ params: payload, headers }
		);
	}

	const lists = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/lists`,
			{ params: payload, headers }
		);
	}

	const listItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/items`,
			{ params: payload, headers }
		);
	}

	const listCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/create`,
			{ params: payload, headers }
		);
	}

	const listDelete = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/delete`,
			{ params: payload, headers }
		);
	}

	const itemCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/create`,
			{ params: payload, headers }
		);
	}

	const itemUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/update`,
			{ params: payload, headers }
		);
	}

	const itemPriority = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/priority`,
			{ params: payload, headers }
		);
	}

	const itemScheduled = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/scheduled`,
			{ params: payload, headers }
		);
	}

	const itemFlagged = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/flagged`,
			{ params: payload, headers }
		);
	}

	const itemCompleted = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/completed`,
			{ params: payload, headers }
		);
	}

	const getToday = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/today`,
			{ params: payload, headers }
		);
	}

	const getScheduled = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/scheduled`,
			{ params: payload, headers }
		);
	}

	const getFlagged = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/flagged`,
			{ params: payload, headers }
		);
	}

	const getAll = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/all`,
			{ params: payload, headers }
		);
	}

	const getLists = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/lists`,
			{ params: payload, headers }
		);
	}

	const getListItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/items`,
			{ params: payload, headers }
		);
	}

	const putCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/create`,
			{ params: payload, headers }
		);
	}

	const putDelete = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/delete`,
			{ params: payload, headers }
		);
	}

	const putItemCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/create`,
			{ params: payload, headers }
		);
	}

	const postItemUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/update`,
			{ params: payload, headers }
		);
	}

	const postItemSetPriority = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/priority`,
			{ params: payload, headers }
		);
	}

	const postItemSetScheduled = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/scheduled`,
			{ params: payload, headers }
		);
	}

	const postItemSetFlagged = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/flagged`,
			{ params: payload, headers }
		);
	}

	const postItemSetCompleted = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/item/set/completed`,
			{ params: payload, headers }
		);
	}

	const getListsMetrics = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/lists/metrics`,
			{ params: payload, headers }
		);
	}

	const putListCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/create`,
			{ params: payload, headers }
		);
	}

	const putListDelete = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/delete`,
			{ params: payload, headers }
		);
	}

	const putListItemCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/create`,
			{ params: payload, headers }
		);
	}

	const postListItemUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['todo.kit']}/api/list/item/update`,
			{ params: payload, headers }
		);
	}

	return { TODOKitData, todayItems, scheduledItems, flaggedItems, allItems, lists, listItems, listCreate, listDelete, itemCreate, itemUpdate, itemPriority, itemScheduled, itemFlagged, itemCompleted, getToday, getScheduled, getFlagged, getAll, getLists, getListItems, putCreate, putDelete, putItemCreate, postItemUpdate, postItemSetPriority, postItemSetScheduled, postItemSetFlagged, postItemSetCompleted, getListsMetrics, putListCreate, putListDelete, putListItemCreate, postListItemUpdate };
}