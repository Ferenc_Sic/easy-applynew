import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function DesignKit() {

	const DesignKitData = ref({});

	const buildInvoice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/build/invoice`,
			{ params: payload, headers }
		);
	}

	const postPdfToText = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/pdf/to/text`,
			{ params: payload, headers }
		);
	}

	const postPdfToCsv = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/pdf/to/csv`,
			{ params: payload, headers }
		);
	}

	const postImagesGdConcatenate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/images/gd/concatenate`,
			{ params: payload, headers }
		);
	}

	const getPdfTemplates = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/templates`,
			{ params: payload, headers }
		);
	}

	const getPdfTemplate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template`,
			{ params: payload, headers }
		);
	}

	const postCreatePdfTemplate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template`,
			{ params: payload, headers }
		);
	}

	const putUpdatePdfTemplate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template`,
			{ params: payload, headers }
		);
	}

	const deletePdfTemplate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template`,
			{ params: payload, headers }
		);
	}

	const postGeneratePdfTemplate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template/generate`,
			{ params: payload, headers }
		);
	}

	const getPdfTemplatesFiltered = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/templates/filtered`,
			{ params: payload, headers }
		);
	}

	const postGeneratePdfFromHtml = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/html/to/pdf`,
			{ params: payload, headers }
		);
	}

	const postGenerateQrCode = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/qr-code/generate`,
			{ params: payload, headers }
		);
	}

	const postImageToText = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/image/to/text`,
			{ params: payload, headers }
		);
	}

	const postWordDocumentToText = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/word/to/text`,
			{ params: payload, headers }
		);
	}

	const postGeneratePdfTemplateExample = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/pdf/template/example`,
			{ params: payload, headers }
		);
	}

	const getGenerateQrCode = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/qr-code/generate`,
			{ params: payload, headers }
		);
	}

	const postGeneratePdfTemplateFromView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['design.kit']}/api/templates/view/to/pdf/generate`,
			{ params: payload, headers }
		);
	}

	return { DesignKitData, buildInvoice, postPdfToText, postPdfToCsv, postImagesGdConcatenate, getPdfTemplates, getPdfTemplate, postCreatePdfTemplate, putUpdatePdfTemplate, deletePdfTemplate, postGeneratePdfTemplate, getPdfTemplatesFiltered, postGeneratePdfFromHtml, postGenerateQrCode, postImageToText, postWordDocumentToText, postGeneratePdfTemplateExample, getGenerateQrCode, postGeneratePdfTemplateFromView };
}