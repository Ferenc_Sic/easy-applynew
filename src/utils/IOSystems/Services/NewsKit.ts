import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function NewsKit() {

	const NewsKitData = ref({});

	const subscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/subscribe`,
			{ params: payload, headers }
		);
	}

	const putSubscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/subscribe`,
			{ params: payload, headers }
		);
	}

	const putUnsubscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/unsubscribe`,
			{ params: payload, headers }
		);
	}

	const postSubscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/subscribe`,
			{ params: payload, headers }
		);
	}

	const postUnsubscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/unsubscribe`,
			{ params: payload, headers }
		);
	}

	const getSubscribers = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['news.kit']}/api/subscribers`,
			{ params: payload, headers }
		);
	}

	return { NewsKitData, subscribe, putSubscribe, putUnsubscribe, postSubscribe, postUnsubscribe, getSubscribers };
}