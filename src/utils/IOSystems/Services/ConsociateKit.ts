import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function ConsociateKit() {

	const ConsociateKitData = ref({});

	const getKonnectkitTeam = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/api/konnectkit.nl/team`,
			{ params: payload, headers }
		);
	}

	const getNetworkContactInformation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/contact/information`,
			{ params: payload, headers }
		);
	}

	const getCurrentContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/current/contacts`,
			{ params: payload, headers }
		);
	}

	const getPotentialContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/potential/contacts`,
			{ params: payload, headers }
		);
	}

	const getSearchContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/search/contacts`,
			{ params: payload, headers }
		);
	}

	const getPendingContactApproval = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/pending/contact/approval`,
			{ params: payload, headers }
		);
	}

	const postAcceptPendingContactApproval = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/accept/pending/contact`,
			{ params: payload, headers }
		);
	}

	const postRefusePendingContactApproval = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/refuse/pending/contact`,
			{ params: payload, headers }
		);
	}

	const postConnectContact = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/connect/contact`,
			{ params: payload, headers }
		);
	}

	const postDisconnectContact = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/disconnect/contact`,
			{ params: payload, headers }
		);
	}

	const postBlockContact = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/block/contact`,
			{ params: payload, headers }
		);
	}

	const postNetworkSettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/set/settings`,
			{ params: payload, headers }
		);
	}

	const getNetworkTimeline = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/timeline/messages`,
			{ params: payload, headers }
		);
	}

	const postNetworkTimeline = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/timeline/add/message`,
			{ params: payload, headers }
		);
	}

	const getNetworkPrivateMessages = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/private/messages`,
			{ params: payload, headers }
		);
	}

	const getDomainChildrenPublicCommunities = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/domain/children/public/communities`,
			{ params: payload, headers }
		);
	}

	const getDomainChildrenFeaturedCommunities = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/domain/children/featured/communities`,
			{ params: payload, headers }
		);
	}

	const getDomainChildrenSearchCommunities = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/domain/children/search/communities`,
			{ params: payload, headers }
		);
	}

	const getCommunity = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/community`,
			{ params: payload, headers }
		);
	}

	const getCommunityEvents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/community/events`,
			{ params: payload, headers }
		);
	}

	const postCurriculumVitaeUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/curriculum/vitae/upload`,
			{ params: payload, headers }
		);
	}

	const postUpdateCurriculumVitae = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/curriculum/vitae/update`,
			{ params: payload, headers }
		);
	}

	const getCurriculumVitaeKeywordSearch = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/curriculum/vitae/keyword/search`,
			{ params: payload, headers }
		);
	}

	const getCurriculumVitaeKeywordSynonymsSearch = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/curriculum/vitae/keyword/synonyms/search`,
			{ params: payload, headers }
		);
	}

	const postDispatchCurriculumVitaeJobAnalysisThread = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/dispatch/curriculum/vitae/job/analysis/thread`,
			{ params: payload, headers }
		);
	}

	const getDomainChildrenCurrentContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/domain/children/network/current/contacts`,
			{ params: payload, headers }
		);
	}

	const getAcquaintedContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/acquainted/contacts`,
			{ params: payload, headers }
		);
	}

	const getInterestingContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/interesting/contacts`,
			{ params: payload, headers }
		);
	}

	const getMatchingContacts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/matching/contacts`,
			{ params: payload, headers }
		);
	}

	const deleteNetworkTimelineMessage = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/network/timeline/remove/message`,
			{ params: payload, headers }
		);
	}

	const getDomainChildrenMemberEvents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/domain/children/member/communities`,
			{ params: payload, headers }
		);
	}

	const postEventMemberRegistration = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/community/member/registration`,
			{ params: payload, headers }
		);
	}

	const getCommunityEventsParticipated = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['consociate.kit']}/api/community/events/participated`,
			{ params: payload, headers }
		);
	}

	return { ConsociateKitData, getKonnectkitTeam, getNetworkContactInformation, getCurrentContacts, getPotentialContacts, getSearchContacts, getPendingContactApproval, postAcceptPendingContactApproval, postRefusePendingContactApproval, postConnectContact, postDisconnectContact, postBlockContact, postNetworkSettings, getNetworkTimeline, postNetworkTimeline, getNetworkPrivateMessages, getDomainChildrenPublicCommunities, getDomainChildrenFeaturedCommunities, getDomainChildrenSearchCommunities, getCommunity, getCommunityEvents, postCurriculumVitaeUpload, postUpdateCurriculumVitae, getCurriculumVitaeKeywordSearch, getCurriculumVitaeKeywordSynonymsSearch, postDispatchCurriculumVitaeJobAnalysisThread, getDomainChildrenCurrentContacts, getAcquaintedContacts, getInterestingContacts, getMatchingContacts, deleteNetworkTimelineMessage, getDomainChildrenMemberEvents, postEventMemberRegistration, getCommunityEventsParticipated };
}