import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function PrivacyKit() {

	const PrivacyKitData = ref({});

	const postEmailConsents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['privacy.kit']}/api/email/consents`,
			{ params: payload, headers }
		);
	}

	return { PrivacyKitData, postEmailConsents };
}