import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function IntegrationKit() {

	const IntegrationKitData = ref({});

	const getInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const postInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const putInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const patchInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.patch(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const deleteInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const optionsInterface = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.options(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/interface/{integration}/{method}`,
			{ params: payload, headers }
		);
	}

	const getIntegrationCouchdb = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['integration.kit']}/api/integration/couchdb/{database}/_all_docs`,
			{ params: payload, headers }
		);
	}

	return { IntegrationKitData, getInterface, postInterface, putInterface, patchInterface, deleteInterface, optionsInterface, getIntegrationCouchdb };
}