import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function MonoKit() {

	const MonoKitData = ref({});

	const postGovwiseGetgrid = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/govwise/getgrid`,
			{ params: payload, headers }
		);
	}

	const postGovwiseGridWithFavorites = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/govwise/grid/with/favorites`,
			{ params: payload, headers }
		);
	}

	const postGovwiseToggleFavorite = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/govwise/toggle/favorite/{uuid}`,
			{ params: payload, headers }
		);
	}

	const postBewittRegistration = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/bewitt/registration`,
			{ params: payload, headers }
		);
	}

	const postBoringwebRandomLink = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/boringweb/random/link`,
			{ params: payload, headers }
		);
	}

	const postBoringwebAccessLink = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/boringweb/access/link`,
			{ params: payload, headers }
		);
	}

	const getBackofficeBewittEvents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/backoffice/bewitt/events`,
			{ params: payload, headers }
		);
	}

	const getBewittStats = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/bewitt/stats`,
			{ params: payload, headers }
		);
	}

	const getBewittChatboxMessages = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/bewitt/chatbox/messages`,
			{ params: payload, headers }
		);
	}

	const getBewittOrganizations = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['mono.kit']}/api/bewitt/organizations`,
			{ params: payload, headers }
		);
	}

	return { MonoKitData, postGovwiseGetgrid, postGovwiseGridWithFavorites, postGovwiseToggleFavorite, postBewittRegistration, postBoringwebRandomLink, postBoringwebAccessLink, getBackofficeBewittEvents, getBewittStats, getBewittChatboxMessages, getBewittOrganizations };
}