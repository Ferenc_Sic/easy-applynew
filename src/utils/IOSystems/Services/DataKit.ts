import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function DataKit() {

	const DataKitData = ref({});

	const postFlexibleDataModelsCreateFromBasicSource = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['data.kit']}//flexible/data/models/create/from/basic/source`,
			{ params: payload, headers }
		);
	}

	const getCountries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['data.kit']}/api/countries`,
			{ params: payload, headers }
		);
	}

	const getLanguages = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['data.kit']}/api/languages`,
			{ params: payload, headers }
		);
	}

	return { DataKitData, postFlexibleDataModelsCreateFromBasicSource, getCountries, getLanguages };
}