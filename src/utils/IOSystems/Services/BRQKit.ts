import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function BRQKit() {

	const BRQKitData = ref({});

	const postTriggerAction = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['brq.kit']}/api/workspace/actions/trigger`,
			{ params: payload, headers }
		);
	}

	const getClickupTask = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['brq.kit']}/api/workspace/clickup/tasks`,
			{ params: payload, headers }
		);
	}

	return { BRQKitData, postTriggerAction, getClickupTask };
}