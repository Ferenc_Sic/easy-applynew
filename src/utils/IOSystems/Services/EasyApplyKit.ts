import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function EasyApplyKit() {

	const EasyApplyKitData = ref({});

	const getJobOpportunity = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/job/opportunity`,
			{ params: payload, headers }
		);
	}

	const getTrendingJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/jobs/trending`,
			{ params: payload, headers }
		);
	}

	const getSearchJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/jobs/search`,
			{ params: payload, headers }
		);
	}

	const getMatchingJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/jobs/matching`,
			{ params: payload, headers }
		);
	}

	const getInternshipJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/jobs/internships`,
			{ params: payload, headers }
		);
	}

	const getRelatedJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/jobs/related`,
			{ params: payload, headers }
		);
	}

	const getCourse = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/course`,
			{ params: payload, headers }
		);
	}

	const getCourses = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/courses`,
			{ params: payload, headers }
		);
	}

	const postAccountAccess = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/account/access`,
			{ params: payload, headers }
		);
	}

	const postAccountAccessConfirmation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/account/access/confirmation`,
			{ params: payload, headers }
		);
	}

	const postCurriculumJobAnalysisThread = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/curriculum/job/analysis/thread`,
			{ params: payload, headers }
		);
	}

	const postCurriculumEmailConfirmation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/curriculum/confirm/email`,
			{ params: payload, headers }
		);
	}

	const getFiltersCountries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/filters/countries`,
			{ params: payload, headers }
		);
	}

	const getFiltersTags = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/filters/tags`,
			{ params: payload, headers }
		);
	}

	const postSubmitJobOpening = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/submit/job/opening`,
			{ params: payload, headers }
		);
	}

	const postApplyNow = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/apply/now`,
			{ params: payload, headers }
		);
	}

	const postRemoveCurriculumVitae = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/account/documents/remove/curriculum/vitae`,
			{ params: payload, headers }
		);
	}

	const getAccountActivity = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/account/activity`,
			{ params: payload, headers }
		);
	}

	const getRecruitersValidateToken = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/validate/token`,
			{ params: payload, headers }
		);
	}

	const getRecruitersIndexedJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/indexed/jobs`,
			{ params: payload, headers }
		);
	}

	const getRecruitersCandidates = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/candidates`,
			{ params: payload, headers }
		);
	}

	const recruitersGetIndexedJobs = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/indexed/jobs/unindex/job`,
			{ params: payload, headers }
		);
	}

	const postCurriculumApprove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/curriculum/approve`,
			{ params: payload, headers }
		);
	}

	const postCurriculumRefuse = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/curriculum/refuse`,
			{ params: payload, headers }
		);
	}

	const postApplicationApprove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/application/approve`,
			{ params: payload, headers }
		);
	}

	const postApplicationRefuse = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/application/refuse`,
			{ params: payload, headers }
		);
	}

	const getV2JobOpenings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/v2/jobs`,
			{ params: payload, headers }
		);
	}

	const getV2Tags = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/v2/tags`,
			{ params: payload, headers }
		);
	}

	const getV2Industries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/v2/industries`,
			{ params: payload, headers }
		);
	}

	const getV2Countries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['easyapply.kit']}/api/v2/countries`,
			{ params: payload, headers }
		);
	}

	return { EasyApplyKitData, getJobOpportunity, getTrendingJobs, getSearchJobs, getMatchingJobs, getInternshipJobs, getRelatedJobs, getCourse, getCourses, postAccountAccess, postAccountAccessConfirmation, postCurriculumJobAnalysisThread, postCurriculumEmailConfirmation, getFiltersCountries, getFiltersTags, postSubmitJobOpening, postApplyNow, postRemoveCurriculumVitae, getAccountActivity, getRecruitersValidateToken, getRecruitersIndexedJobs, getRecruitersCandidates, recruitersGetIndexedJobs, postCurriculumApprove, postCurriculumRefuse, postApplicationApprove, postApplicationRefuse, getV2JobOpenings, getV2Tags, getV2Industries, getV2Countries };
}