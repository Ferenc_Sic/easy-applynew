import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function LabKit() {

	const LabKitData = ref({});

	return { LabKitData,  };
}