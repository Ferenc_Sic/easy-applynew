import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function EmailKit() {

	const EmailKitData = ref({});

	const rawEmail = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/raw`,
			{ params: payload, headers }
		);
	}

	const sendEmail = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/send`,
			{ params: payload, headers }
		);
	}

	const queueEmail = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/queue`,
			{ params: payload, headers }
		);
	}

	const postSend = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/send`,
			{ params: payload, headers }
		);
	}

	const postRaw = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/raw`,
			{ params: payload, headers }
		);
	}

	const postHtml = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/html`,
			{ params: payload, headers }
		);
	}

	const postQueue = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/queue`,
			{ params: payload, headers }
		);
	}

	const postBulkQueue = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/bulk/queue`,
			{ params: payload, headers }
		);
	}

	const postHelpersProcessEml = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/helpers/process/eml`,
			{ params: payload, headers }
		);
	}

	const getHelpersEmlJson = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['email.mykit.ws']}/api/helpers/retrieve/eml/json`,
			{ params: payload, headers }
		);
	}

	return { EmailKitData, rawEmail, sendEmail, queueEmail, postSend, postRaw, postHtml, postQueue, postBulkQueue, postHelpersProcessEml, getHelpersEmlJson };
}