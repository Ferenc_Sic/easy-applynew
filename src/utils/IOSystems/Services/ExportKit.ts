import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function ExportKit() {

	const ExportKitData = ref({});

	const exportSimpleDataset = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['export.kit']}/api/export/simple/dataset`,
			{ params: payload, headers }
		);
	}

	const postExportCsv = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['export.kit']}/api/export/csv`,
			{ params: payload, headers }
		);
	}

	const postExportSimpleDataset = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['export.kit']}/api/export/simple/dataset`,
			{ params: payload, headers }
		);
	}

	const postExportViewDataset = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['export.kit']}/api/export/view/dataset`,
			{ params: payload, headers }
		);
	}

	return { ExportKitData, exportSimpleDataset, postExportCsv, postExportSimpleDataset, postExportViewDataset };
}