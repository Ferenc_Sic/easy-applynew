import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function StorageKit() {

	const StorageKitData = ref({});

	const getFolders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/folders`,
			{ params: payload, headers }
		);
	}

	const getFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const putFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const postFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const deleteFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const getAllFiles = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/files`,
			{ params: payload, headers }
		);
	}

	const getFile = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/file`,
			{ params: payload, headers }
		);
	}

	const postFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/file/upload`,
			{ params: payload, headers }
		);
	}

	const postBulkFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/bulk/file/upload`,
			{ params: payload, headers }
		);
	}

	const postRoleProtectedFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/role/protected/file/upload`,
			{ params: payload, headers }
		);
	}

	const postRoleProtectedBulkFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/role/protected/bulk/file/upload`,
			{ params: payload, headers }
		);
	}

	const postCredentialProtectedFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/credential/protected/file/upload`,
			{ params: payload, headers }
		);
	}

	const postCredentialProtectedBulkFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/credential/protected/bulk/file/upload`,
			{ params: payload, headers }
		);
	}

	const postOrganizationProtectedFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/organization/protected/file/upload`,
			{ params: payload, headers }
		);
	}

	const postOrganizationProtectedBulkFileUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/organization/protected/bulk/file/upload`,
			{ params: payload, headers }
		);
	}

	const postHandleCallableUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['storage.kit']}/api/handle/callable/upload`,
			{ params: payload, headers }
		);
	}

	return { StorageKitData, getFolders, getFolder, putFolder, postFolder, deleteFolder, getAllFiles, getFile, postFileUpload, postBulkFileUpload, postRoleProtectedFileUpload, postRoleProtectedBulkFileUpload, postCredentialProtectedFileUpload, postCredentialProtectedBulkFileUpload, postOrganizationProtectedFileUpload, postOrganizationProtectedBulkFileUpload, postHandleCallableUpload };
}