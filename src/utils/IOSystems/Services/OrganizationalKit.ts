import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function OrganizationalKit() {

	const OrganizationalKitData = ref({});

	const getCompany = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/company`,
			{ params: payload, headers }
		);
	}

	const getOwnedCompany = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company`,
			{ params: payload, headers }
		);
	}

	const putCompany = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/company`,
			{ params: payload, headers }
		);
	}

	const putOwnedCompany = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company`,
			{ params: payload, headers }
		);
	}

	const getCompanyStakeholders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/company/stakeholders`,
			{ params: payload, headers }
		);
	}

	const getOwnedCompanyStakeholders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company/stakeholders`,
			{ params: payload, headers }
		);
	}

	const putCompanyStakeholder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/company/stakeholder`,
			{ params: payload, headers }
		);
	}

	const putOwnedCompanyStakeholder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company/stakeholder`,
			{ params: payload, headers }
		);
	}

	const getOwnedCompanySettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company/settings`,
			{ params: payload, headers }
		);
	}

	const putOwnedCompanySettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['organizational.kit']}/api/owned/company/settings`,
			{ params: payload, headers }
		);
	}

	return { OrganizationalKitData, getCompany, getOwnedCompany, putCompany, putOwnedCompany, getCompanyStakeholders, getOwnedCompanyStakeholders, putCompanyStakeholder, putOwnedCompanyStakeholder, getOwnedCompanySettings, putOwnedCompanySettings };
}