import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function IDKit() {

	const IDKitData = ref({});

	const register = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/register`,
			{ params: payload, headers }
		);
	}

	const authenticate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/auth`,
			{ params: payload, headers }
		);
	}

	const me = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/me`,
			{ params: payload, headers }
		);
	}

	const putContactInformation = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/contact/information`,
			{ params: payload, headers }
		);
	}

	const myKitUsers = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/list/of/users`,
			{ params: payload, headers }
		);
	}

	const confirmCredentials = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/confirm/:token`,
			{ params: payload, headers }
		);
	}

	const getMe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/me`,
			{ params: payload, headers }
		);
	}

	const postAuth = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/auth`,
			{ params: payload, headers }
		);
	}

	const postRegister = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/register`,
			{ params: payload, headers }
		);
	}

	const postDeviceTokenAuthentication = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/device/token`,
			{ params: payload, headers }
		);
	}

	const getConfirmCredentials = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/confirm/{token}`,
			{ params: payload, headers }
		);
	}

	const putContactInformationSettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/contact/information/settings`,
			{ params: payload, headers }
		);
	}

	const putContactInformationSocialNetworks = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/contact/information/social-networks`,
			{ params: payload, headers }
		);
	}

	const getListOfUsers = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['id.kit']}/api/list/of/users`,
			{ params: payload, headers }
		);
	}

	return { IDKitData, register, authenticate, me, putContactInformation, myKitUsers, confirmCredentials, getMe, postAuth, postRegister, postDeviceTokenAuthentication, getConfirmCredentials, putContactInformationSettings, putContactInformationSocialNetworks, getListOfUsers };
}