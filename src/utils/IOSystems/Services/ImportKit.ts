import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function ImportKit() {

	const ImportKitData = ref({});

	const postUploadImportFile = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['import.kit']}/api/upload/import/file`,
			{ params: payload, headers }
		);
	}

	const postSaveMappingImport = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['import.kit']}/api/save/mapping/import`,
			{ params: payload, headers }
		);
	}

	return { ImportKitData, postUploadImportFile, postSaveMappingImport };
}