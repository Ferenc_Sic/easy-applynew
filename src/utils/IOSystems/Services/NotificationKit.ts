import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function NotificationKit() {

	const NotificationKitData = ref({});

	const postSync = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['notifications.kit']}/api/sync`,
			{ params: payload, headers }
		);
	}

	const postDevicesIosRegister = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['notifications.kit']}/api/devices/ios/register`,
			{ params: payload, headers }
		);
	}

	const postDevicesIosUnregister = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['notifications.kit']}/api/devices/ios/unregister`,
			{ params: payload, headers }
		);
	}

	return { NotificationKitData, postSync, postDevicesIosRegister, postDevicesIosUnregister };
}