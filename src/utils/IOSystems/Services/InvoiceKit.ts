import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function InvoiceKit() {

	const InvoiceKitData = ref({});

	const putInvoice = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/invoice`,
			{ params: payload, headers }
		);
	}

	const getInvoices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/invoices`,
			{ params: payload, headers }
		);
	}

	const getDomainInvoices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/domain/invoices`,
			{ params: payload, headers }
		);
	}

	const getCompanyInvoices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/company/invoices`,
			{ params: payload, headers }
		);
	}

	const getOwnedCompanyInvoices = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/owned/company/invoices`,
			{ params: payload, headers }
		);
	}

	const postSendInvoicesByEmail = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/send/invoices/pdf/email`,
			{ params: payload, headers }
		);
	}

	const getFormatterNextInvoiceIdentifier = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/formatter/next/invoice/identifier`,
			{ params: payload, headers }
		);
	}

	const getInvoiceXml = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['invoice.kit']}/api/invoice/xml`,
			{ params: payload, headers }
		);
	}

	return { InvoiceKitData, putInvoice, getInvoices, getDomainInvoices, getCompanyInvoices, getOwnedCompanyInvoices, postSendInvoicesByEmail, getFormatterNextInvoiceIdentifier, getInvoiceXml };
}