import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function NoteKit() {

	const NoteKitData = ref({});

	const getNotes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/notes`,
			{ params: payload, headers }
		);
	}

	const getNote = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/note`,
			{ params: payload, headers }
		);
	}

	const putNote = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/note`,
			{ params: payload, headers }
		);
	}

	const postUpdateNote = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/note`,
			{ params: payload, headers }
		);
	}

	const deleteNote = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/note`,
			{ params: payload, headers }
		);
	}

	const getFolders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/folders`,
			{ params: payload, headers }
		);
	}

	const getFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const putFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const postUpdateFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	const deleteFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['note.kit']}/api/folder`,
			{ params: payload, headers }
		);
	}

	return { NoteKitData, getNotes, getNote, putNote, postUpdateNote, deleteNote, getFolders, getFolder, putFolder, postUpdateFolder, deleteFolder };
}