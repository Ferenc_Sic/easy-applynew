import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function GamificationKit() {

	const GamificationKitData = ref({});

	const challenges = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenges`,
			{ params: payload, headers }
		);
	}

	const quizzes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quizzes`,
			{ params: payload, headers }
		);
	}

	const leaderboards = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboards`,
			{ params: payload, headers }
		);
	}

	const leaders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboard/leaders`,
			{ params: payload, headers }
		);
	}

	const me = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/me`,
			{ params: payload, headers }
		);
	}

	const createChallenge = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenge/create`,
			{ params: payload, headers }
		);
	}

	const createAchievement = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/achievement/create`,
			{ params: payload, headers }
		);
	}

	const leaderboardActivate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboard/activate`,
			{ params: payload, headers }
		);
	}

	const leaderboardDeactivate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboard/deactivate`,
			{ params: payload, headers }
		);
	}

	const createQuizGroup = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/group/create`,
			{ params: payload, headers }
		);
	}

	const createQuizQuestion = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/question/create`,
			{ params: payload, headers }
		);
	}

	const updateChallenge = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenge/update`,
			{ params: payload, headers }
		);
	}

	const triggerAction = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/trigger/action`,
			{ params: payload, headers }
		);
	}

	const redeemProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/redeem/product`,
			{ params: payload, headers }
		);
	}

	const triggerGamificationClone = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/gamification/clone`,
			{ params: payload, headers }
		);
	}

	const getLeaderboards = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboards`,
			{ params: payload, headers }
		);
	}

	const getLeaders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboard/leaders`,
			{ params: payload, headers }
		);
	}

	const getChallenges = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenges`,
			{ params: payload, headers }
		);
	}

	const getAchievements = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/achievements`,
			{ params: payload, headers }
		);
	}

	const getAllQuizzes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quizzes`,
			{ params: payload, headers }
		);
	}

	const getQuizzes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quizzes/available`,
			{ params: payload, headers }
		);
	}

	const getQuizGroups = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/groups`,
			{ params: payload, headers }
		);
	}

	const getQuizQuestions = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/questions`,
			{ params: payload, headers }
		);
	}

	const getCompletedChallenges = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenges/completed`,
			{ params: payload, headers }
		);
	}

	const getCompletedQuizzes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quizzes/completed`,
			{ params: payload, headers }
		);
	}

	const getCompletedQuestions = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/questions/completed`,
			{ params: payload, headers }
		);
	}

	const getCompletedAchievements = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/achievements/completed`,
			{ params: payload, headers }
		);
	}

	const getRedeemedProducts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/redeemed/products`,
			{ params: payload, headers }
		);
	}

	const getQuizQuestion = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/question`,
			{ params: payload, headers }
		);
	}

	const postQuizQuestionAnswer = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/question/answer`,
			{ params: payload, headers }
		);
	}

	const postTransact = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/transact`,
			{ params: payload, headers }
		);
	}

	const postCreateChallenge = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenge/create`,
			{ params: payload, headers }
		);
	}

	const postCreateAchievement = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/achievement/create`,
			{ params: payload, headers }
		);
	}

	const postCreateQuizGroup = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/group/create`,
			{ params: payload, headers }
		);
	}

	const postCreateQuizQuestion = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/question/create`,
			{ params: payload, headers }
		);
	}

	const postChallengeUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/challenge/update`,
			{ params: payload, headers }
		);
	}

	const postAchievementUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/achievement/update`,
			{ params: payload, headers }
		);
	}

	const postLeaderboardUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/leaderboard/update`,
			{ params: payload, headers }
		);
	}

	const postTriggerAction = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/trigger/action`,
			{ params: payload, headers }
		);
	}

	const postRedeemProduct = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/redeem/product`,
			{ params: payload, headers }
		);
	}

	const postGamificationClone = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/gamification/clone`,
			{ params: payload, headers }
		);
	}

	const getActionableQrCode = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/actionable/qr-code`,
			{ params: payload, headers }
		);
	}

	const postTriggerActionableQrCode = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/trigger/actionable/qr-code`,
			{ params: payload, headers }
		);
	}

	const getRealTimeQuestions = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/real-time/questions`,
			{ params: payload, headers }
		);
	}

	const getRealTimeListenQrCodeRead = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/real-time/listen/qr/code/read`,
			{ params: payload, headers }
		);
	}

	const getMyGamification = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/my-gamification`,
			{ params: payload, headers }
		);
	}

	const postUpdateQuizGroup = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/group/update`,
			{ params: payload, headers }
		);
	}

	const postUpdateQuizQuestion = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/quiz/question/update`,
			{ params: payload, headers }
		);
	}

	const getRealTimeReleasedQuestion = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['gamification.kit']}/api/real-time/released/question`,
			{ params: payload, headers }
		);
	}

	return { GamificationKitData, challenges, quizzes, leaderboards, leaders, me, createChallenge, createAchievement, leaderboardActivate, leaderboardDeactivate, createQuizGroup, createQuizQuestion, updateChallenge, triggerAction, redeemProduct, triggerGamificationClone, getLeaderboards, getLeaders, getChallenges, getAchievements, getAllQuizzes, getQuizzes, getQuizGroups, getQuizQuestions, getCompletedChallenges, getCompletedQuizzes, getCompletedQuestions, getCompletedAchievements, getRedeemedProducts, getQuizQuestion, postQuizQuestionAnswer, postTransact, postCreateChallenge, postCreateAchievement, postCreateQuizGroup, postCreateQuizQuestion, postChallengeUpdate, postAchievementUpdate, postLeaderboardUpdate, postTriggerAction, postRedeemProduct, postGamificationClone, getActionableQrCode, postTriggerActionableQrCode, getRealTimeQuestions, getRealTimeListenQrCodeRead, getMyGamification, postUpdateQuizGroup, postUpdateQuizQuestion, getRealTimeReleasedQuestion };
}