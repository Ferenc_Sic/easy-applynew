import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function AgendaKit() {

	const AgendaKitData = ref({});

	const getScheduledDays = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/days`,
			{ params: payload, headers }
		);
	}

	const getScheduledItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/items`,
			{ params: payload, headers }
		);
	}

	const getDailyScheduledItems = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/items/daily`,
			{ params: payload, headers }
		);
	}

	const getRatings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/rates`,
			{ params: payload, headers }
		);
	}

	const getFeedbacks = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/feedbacks`,
			{ params: payload, headers }
		);
	}

	const rateScheduledItem = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/rate`,
			{ params: payload, headers }
		);
	}

	const feedbackScheduledItem = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/feedback`,
			{ params: payload, headers }
		);
	}

	const organizeMeeting = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/meeting/organize`,
			{ params: payload, headers }
		);
	}

	const putScheduledItem = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/create`,
			{ params: payload, headers }
		);
	}

	const updateScheduledItem = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/update`,
			{ params: payload, headers }
		);
	}

	const removeScheduledItem = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/remove`,
			{ params: payload, headers }
		);
	}

	const getEvents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/events`,
			{ params: payload, headers }
		);
	}

	const getEvent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event`,
			{ params: payload, headers }
		);
	}

	const getOwnerEvents = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/owner/events`,
			{ params: payload, headers }
		);
	}

	const putEvent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/create`,
			{ params: payload, headers }
		);
	}

	const updateEvent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/update`,
			{ params: payload, headers }
		);
	}

	const removeEvent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/remove`,
			{ params: payload, headers }
		);
	}

	const getScheduledDaily = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/items/daily`,
			{ params: payload, headers }
		);
	}

	const getRates = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/rates`,
			{ params: payload, headers }
		);
	}

	const postScheduledItemRate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/rate`,
			{ params: payload, headers }
		);
	}

	const postScheduledItemFeedback = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/feedback`,
			{ params: payload, headers }
		);
	}

	const postMeetingOrganize = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/meeting/organize`,
			{ params: payload, headers }
		);
	}

	const putScheduledItemCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/create`,
			{ params: payload, headers }
		);
	}

	const postScheduledItemUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/update`,
			{ params: payload, headers }
		);
	}

	const deleteScheduledItemRemove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/scheduled/item/remove`,
			{ params: payload, headers }
		);
	}

	const getEventsByOwner = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/owner/events`,
			{ params: payload, headers }
		);
	}

	const putEventCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/create`,
			{ params: payload, headers }
		);
	}

	const postEventUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/update`,
			{ params: payload, headers }
		);
	}

	const deleteEventRemove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/remove`,
			{ params: payload, headers }
		);
	}

	const postEventCheckin = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['agenda.kit']}/api/event/checkin`,
			{ params: payload, headers }
		);
	}

	return { AgendaKitData, getScheduledDays, getScheduledItems, getDailyScheduledItems, getRatings, getFeedbacks, rateScheduledItem, feedbackScheduledItem, organizeMeeting, putScheduledItem, updateScheduledItem, removeScheduledItem, getEvents, getEvent, getOwnerEvents, putEvent, updateEvent, removeEvent, getScheduledDaily, getRates, postScheduledItemRate, postScheduledItemFeedback, postMeetingOrganize, putScheduledItemCreate, postScheduledItemUpdate, deleteScheduledItemRemove, getEventsByOwner, putEventCreate, postEventUpdate, deleteEventRemove, postEventCheckin };
}