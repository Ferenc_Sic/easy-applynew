import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function DataViewKit() {

	const DataViewKitData = ref({});

	const putQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const retrieveQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const retrieveQueries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/queries`,
			{ params: payload, headers }
		);
	}

	const updateQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const deleteQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const getQueryData = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query/data`,
			{ params: payload, headers }
		);
	}

	const getQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const getQueries = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/queries`,
			{ params: payload, headers }
		);
	}

	const postQuery = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/query`,
			{ params: payload, headers }
		);
	}

	const putView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/view`,
			{ params: payload, headers }
		);
	}

	const getView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/view`,
			{ params: payload, headers }
		);
	}

	const getViews = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/views`,
			{ params: payload, headers }
		);
	}

	const postView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/view`,
			{ params: payload, headers }
		);
	}

	const deleteView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.delete(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/view`,
			{ params: payload, headers }
		);
	}

	const getViewData = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['dataview.kit']}/api/view/data`,
			{ params: payload, headers }
		);
	}

	return { DataViewKitData, putQuery, retrieveQuery, retrieveQueries, updateQuery, deleteQuery, getQueryData, getQuery, getQueries, postQuery, putView, getView, getViews, postView, deleteView, getViewData };
}