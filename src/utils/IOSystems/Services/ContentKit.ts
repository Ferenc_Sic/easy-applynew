import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function ContentKit() {

	const ContentKitData = ref({});

	const getTranslationsUniverseLabels = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/universe/labels`,
			{ params: payload, headers }
		);
	}

	const getTranslationsUniverseFrontendLabels = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/universe/frontend/labels`,
			{ params: payload, headers }
		);
	}

	const postTranslationsUniverseLabelsSync = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/universe/labels/sync`,
			{ params: payload, headers }
		);
	}

	const postTranslationsUniverseLabelsMerge = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/universe/labels/merge`,
			{ params: payload, headers }
		);
	}

	const postTranslationsUniverseLabelsUpload = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/universe/labels/upload`,
			{ params: payload, headers }
		);
	}

	const getPage = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/page`,
			{ params: payload, headers }
		);
	}

	const getPages = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/pages`,
			{ params: payload, headers }
		);
	}

	const postPageUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/page/update`,
			{ params: payload, headers }
		);
	}

	const postPageRemove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/page/remove`,
			{ params: payload, headers }
		);
	}

	const getBlogPost = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/blog/post`,
			{ params: payload, headers }
		);
	}

	const getBlogPosts = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/blog/posts`,
			{ params: payload, headers }
		);
	}

	const postBlogPostCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/blog/post/create`,
			{ params: payload, headers }
		);
	}

	const postBlogPostUpdate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/blog/post/update`,
			{ params: payload, headers }
		);
	}

	const postBlogPostRemove = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['content.kit']}/api/blog/post/remove`,
			{ params: payload, headers }
		);
	}

	return { ContentKitData, getTranslationsUniverseLabels, getTranslationsUniverseFrontendLabels, postTranslationsUniverseLabelsSync, postTranslationsUniverseLabelsMerge, postTranslationsUniverseLabelsUpload, getPage, getPages, postPageUpdate, postPageRemove, getBlogPost, getBlogPosts, postBlogPostCreate, postBlogPostUpdate, postBlogPostRemove };
}