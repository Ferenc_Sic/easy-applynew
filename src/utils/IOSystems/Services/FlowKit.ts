import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function FlowKit() {

	const FlowKitData = ref({});

	const bewittRollout = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/bewitt/handle/payment`,
			{ params: payload, headers }
		);
	}

	const postBewittHandleEvent = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/bewitt/handle/event`,
			{ params: payload, headers }
		);
	}

	const postBewittHandlePaymentCallback = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/bewitt/handle/payment`,
			{ params: payload, headers }
		);
	}

	const postCmtHandleSendReminders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/handle/send/reminders`,
			{ params: payload, headers }
		);
	}

	const postCmtHandleSendReminder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/handle/send/reminder`,
			{ params: payload, headers }
		);
	}

	const postCmtReminderFlows = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/reminder/flows`,
			{ params: payload, headers }
		);
	}

	const getCmtReminderFlows = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/reminder/flows`,
			{ params: payload, headers }
		);
	}

	const getCmtAvailableReminders = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/available/reminders`,
			{ params: payload, headers }
		);
	}

	const getBewittEventMetrics = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/bewitt/event/metrics`,
			{ params: payload, headers }
		);
	}

	const postEasyapplyPtHandleJobApplication = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/easyapply/pt/handle/job/application`,
			{ params: payload, headers }
		);
	}

	const postEasyapplyNlHandleJobApplication = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/easyapply/nl/handle/job/application`,
			{ params: payload, headers }
		);
	}

	const postCmtAuthorizedDomains = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/authorized/domains`,
			{ params: payload, headers }
		);
	}

	const getCmtAuthorizedDomains = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['flow.kit']}/api/cmt/authorized/domains`,
			{ params: payload, headers }
		);
	}

	return { FlowKitData, bewittRollout, postBewittHandleEvent, postBewittHandlePaymentCallback, postCmtHandleSendReminders, postCmtHandleSendReminder, postCmtReminderFlows, getCmtReminderFlows, getCmtAvailableReminders, getBewittEventMetrics, postEasyapplyPtHandleJobApplication, postEasyapplyNlHandleJobApplication, postCmtAuthorizedDomains, getCmtAuthorizedDomains };
}