import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function DomainKit() {

	const DomainKitData = ref({});

	const putDomain = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['domain.kit']}/api/domain`,
			{ params: payload, headers }
		);
	}

	const putSettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['domain.kit']}/api/domain/settings`,
			{ params: payload, headers }
		);
	}

	const putDomainCreate = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['domain.kit']}/api/domain`,
			{ params: payload, headers }
		);
	}

	const putDomainSettings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['domain.kit']}/api/domain/settings`,
			{ params: payload, headers }
		);
	}

	const getDomainAvailability = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['domain.kit']}/api/domain/availability`,
			{ params: payload, headers }
		);
	}

	return { DomainKitData, putDomain, putSettings, putDomainCreate, putDomainSettings, getDomainAvailability };
}