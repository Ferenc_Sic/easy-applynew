import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function AssembleKit() {

	const AssembleKitData = ref({});

	const doBewittThings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['assemble.kit']}/api/do/bewitt/things/for/production`,
			{ params: payload, headers }
		);
	}

	const getDoBewittThings = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['assemble.kit']}/api/do/bewitt/things/for/production`,
			{ params: payload, headers }
		);
	}

	return { AssembleKitData, doBewittThings, getDoBewittThings };
}