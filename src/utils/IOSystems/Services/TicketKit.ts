import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function TicketKit() {

	const TicketKitData = ref({});

	const subscribe = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/ticket/account/flow`,
			{ params: payload, headers }
		);
	}

	const generateTickets = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/tickets/generate`,
			{ params: payload, headers }
		);
	}

	const postAccountFlow = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/ticket/account/flow`,
			{ params: payload, headers }
		);
	}

	const postAuthenticationFlow = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/authentication/flow`,
			{ params: payload, headers }
		);
	}

	const getTicket = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/ticket`,
			{ params: payload, headers }
		);
	}

	const postGenerateTickets = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['ticket.kit']}/api/tickets/generate`,
			{ params: payload, headers }
		);
	}

	return { TicketKitData, subscribe, generateTickets, postAccountFlow, postAuthenticationFlow, getTicket, postGenerateTickets };
}