import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function MediaKit() {

	const MediaKitData = ref({});

	const uploadFiles = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/upload/files`,
			{ params: payload, headers }
		);
	}

	const postAddNewFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/repository/add-new-folder`,
			{ params: payload, headers }
		);
	}

	const postUploadFile = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/repository/upload/file`,
			{ params: payload, headers }
		);
	}

	const postSearch = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/repository/search`,
			{ params: payload, headers }
		);
	}

	const postDeleteFile = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/repository/delete-file`,
			{ params: payload, headers }
		);
	}

	const postDeleteFolder = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/repository/delete-folder`,
			{ params: payload, headers }
		);
	}

	const postUploadFiles = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['media.kit']}/api/upload/files`,
			{ params: payload, headers }
		);
	}

	return { MediaKitData, uploadFiles, postAddNewFolder, postUploadFile, postSearch, postDeleteFile, postDeleteFolder, postUploadFiles };
}