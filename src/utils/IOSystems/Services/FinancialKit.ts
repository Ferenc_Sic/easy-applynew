import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function FinancialKit() {

	const FinancialKitData = ref({});

	const getPaymentPlanBases = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-plan/bases`,
			{ params: payload, headers }
		);
	}

	const getPaymentPromiseBases = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-promise/bases`,
			{ params: payload, headers }
		);
	}

	const getDisputeBases = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/dispute/bases`,
			{ params: payload, headers }
		);
	}

	const getPaymentPlan = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-plan`,
			{ params: payload, headers }
		);
	}

	const getPaymentPromise = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-promise`,
			{ params: payload, headers }
		);
	}

	const getDispute = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/dispute`,
			{ params: payload, headers }
		);
	}

	const getPaymentPlans = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-plans`,
			{ params: payload, headers }
		);
	}

	const getPaymentPromises = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-promises`,
			{ params: payload, headers }
		);
	}

	const getDisputes = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/disputes`,
			{ params: payload, headers }
		);
	}

	const putPaymentPlan = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-plan`,
			{ params: payload, headers }
		);
	}

	const putPaymentPromise = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-promise`,
			{ params: payload, headers }
		);
	}

	const putDispute = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/dispute`,
			{ params: payload, headers }
		);
	}

	const postUpdatePaymentPlan = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-plan`,
			{ params: payload, headers }
		);
	}

	const postUpdatePaymentPromise = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payment-promise`,
			{ params: payload, headers }
		);
	}

	const postUpdateDispute = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/dispute`,
			{ params: payload, headers }
		);
	}

	const putCollectionCase = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.put(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/collection/case`,
			{ params: payload, headers }
		);
	}

	const postUpdateCollectionCase = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/collection/case`,
			{ params: payload, headers }
		);
	}

	const postCalculateCollectionCaseInterests = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/collection/case/interests`,
			{ params: payload, headers }
		);
	}

	const postPaymentsUpdateScheduledPayment = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['financial.kit']}/api/payments/scheduled/payment`,
			{ params: payload, headers }
		);
	}

	return { FinancialKitData, getPaymentPlanBases, getPaymentPromiseBases, getDisputeBases, getPaymentPlan, getPaymentPromise, getDispute, getPaymentPlans, getPaymentPromises, getDisputes, putPaymentPlan, putPaymentPromise, putDispute, postUpdatePaymentPlan, postUpdatePaymentPromise, postUpdateDispute, putCollectionCase, postUpdateCollectionCase, postCalculateCollectionCaseInterests, postPaymentsUpdateScheduledPayment };
}