import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function StreamKit() {

	const StreamKitData = ref({});

	const getNow = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['stream.kit']}/api/now`,
			{ params: payload, headers }
		);
	}

	const getStreamsTimeline = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['stream.kit']}/api/streams/timeline`,
			{ params: payload, headers }
		);
	}

	const postRecordLiveStreamView = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['stream.kit']}/api/record/live/stream/view`,
			{ params: payload, headers }
		);
	}

	const postGoLive = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['stream.kit']}/api/go/live`,
			{ params: payload, headers }
		);
	}

	return { StreamKitData, getNow, getStreamsTimeline, postRecordLiveStreamView, postGoLive };
}