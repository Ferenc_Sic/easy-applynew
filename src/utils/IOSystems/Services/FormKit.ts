import { ref, computed } from 'vue';
import axios from 'axios';
import IOSystemsEnvironment from '@/utils/IOSystems/Environment/IOSystemsEnvironment';
const { IOSystemsEnvironmentData } = IOSystemsEnvironment();

export default function FormKit() {

	const FormKitData = ref({});

	const postContactForm = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['form.kit']}/api/contact/form`,
			{ params: payload, headers }
		);
	}

	const postGenericForm = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['form.kit']}/api/generic/form`,
			{ params: payload, headers }
		);
	}

	const postFormBuilderFormSubmit = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.post(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['form.kit']}/api/form/builder/form/submit`,
			{ params: payload, headers }
		);
	}

	const getFormBuilderFormSubmissions = (payload = {}, headers = {}) => {
		payload = { ...payload };
		return axios.get(
			`${IOSystemsEnvironmentData.value.Protocol}//${IOSystemsEnvironmentData.value.Endpoints['form.kit']}/api/form/builder/form/submissions`,
			{ params: payload, headers }
		);
	}

	return { FormKitData, postContactForm, postGenericForm, postFormBuilderFormSubmit, getFormBuilderFormSubmissions };
}