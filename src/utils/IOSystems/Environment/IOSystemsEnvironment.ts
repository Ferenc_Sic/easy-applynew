import { ref, computed } from 'vue';


export default function IOSystemsEnvironment() {

    const EnvironmentSet = require(`./sets/${process.env.NODE_ENV}`)

    const IOSystemsEnvironmentData = ref(EnvironmentSet);

    return { IOSystemsEnvironmentData };

}