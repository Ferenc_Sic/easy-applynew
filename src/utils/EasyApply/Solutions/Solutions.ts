import { ref, computed } from 'vue';

export default function Solutions() {

    const SolutionsData = ref({
        JobSeekers: {
            job_search: {
                title: 'Searching my next <br> dream job',
                description: 'Discover new job opportunities and take the next step in your career with our job search platform. Browse through a wide range of job listings across different industries and countries, tailor your search to fit your specific needs, and apply for your dream job today.',
                route: 'JobSeekersJobSearch',
                pricing: 'Free',
            },
            cv_builder: {
                title: 'Building my <br> Curriculum Vitae',
                description: 'Your curriculum vitae is a representation of your professional self. It\'s a chance to showcase your skills, experience, and achievements to potential employers. By crafting a strong CV, you can open doors to exciting job opportunities and take a step forward in your career.',
                route: '',
                pricing: {
                    amount: 2.99,
                    currency: 'EUR',
                },
            },
            career_advice: {
                title: 'Career advice',
                description: 'Our expertly curated career advice provides valuable insights and guidance to help you navigate the professional landscape with confidence. From identifying your strengths to honing your interview skills, our resources are designed to support you at every stage of your career journey. Unlock your potential.',
                route: '',
                pricing: {
                    amount: 49.99,
                    currency: 'EUR',
                },
            },

            cv_search: {
                title: 'PowerUp: Search <br/>with Curriculum Vitae',
                description: 'Search with Curriculum Vitae enhances your job search by using your CV\'s experience to find tailored opportunities. Our advanced algorithms ensure you discover relevant and exciting career options, streamlining your search and boosting your chances of finding the perfect match.',
                route: '',
                pricing: {
                    amount: 0.99,
                    currency: 'EUR',
                },
            },
        },
        Employers: {
            list_jobs: {
                title: 'List your <br/> job openings',
                description: 'Reach the right candidates and fill your open positions quickly by posting your job listing on our platform. Our user-friendly interface makes it easy to create and manage job postings, and our targeted reach ensures your listing is seen by the most qualified candidates.',
                route: '',
                pricing: {
                    amount: 14.99,
                    currency: 'EUR',
                    conditional: '30 days'
                },
            },
            discover_n_connect: {
                title: 'Connect with <br/> top talent',
                description: 'Easily find exceptional candidates seeking employment in your industry. Connect with top talent, streamline recruitment, and build a winning team to drive success and growth for your business.',
                route: '',
                pricing: {
                    amount: 99.99,
                    currency: 'EUR',
                    conditional: 'month'
                },
            },
            company_profile: {
                title: 'Build your<br/> company profile',
                description: 'Attract top talent with a compelling company profile that showcases your organization\'s culture, values, and mission. A well-crafted profile strengthens your employer brand, resonates with job seekers, and drives interest in your job openings.',
                route: '',
                pricing: {
                    amount: 9.99,
                    currency: 'EUR',
                    conditional: 'month'
                },
            },
            cv_data_structure: {
                title: 'Structure your <br/>CV repository.',
                description: 'Transform your CV collection into a structured dataset with our advanced service. We process and organize the provided CVs, allowing you to easily evaluate and select candidates with a data-driven approach.',
                route: '',
                pricing: {
                    amount: 0.99,
                    currency: 'EUR',
                    conditional: 'CV'
                },
            },
        }
    });

    return { SolutionsData }

}