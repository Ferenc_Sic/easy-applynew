import { ref, computed } from 'vue';

export default function EasyApplyTeam() {

    const EasyApplyTeamData = ref({
        Team: [
            {
                name: 'João Morais',
                role: 'Product Mischief',
                role_description: 'Our Product Mischief leads the charge in creative and innovative product development. With a keen eye for disruption and a playful spirit, João ensures that our platform stays ahead of the curve and offers exciting new features to delight our users.',
                email: 'joao@easyapply.app',
                phone: null,
                linkedin: {
                    label: '/joaomorais94',
                    url: 'https://www.linkedin.com/in/joaomorais94/'
                },
                avatar: '/static/images/team/joao.png',
            },

            {
                name: 'Pedro Fontela',
                role: 'Executive Officer',
                role_description: 'Our CEO, with a background in psychology and diplomacy, is the guiding force behind EasyApply. Their expertise in understanding human behavior and building relationships helps shape the business structure and strategy. The CEO fosters a supportive company culture and ensures that EasyApply remains a leader in connecting job seekers and recruiters worldwide.',
                email: 'pedro@easyapply.app',
                phone: null,
                linkedin: null,
                avatar: '/static/images/team/joao.png',
            },

            {
                name: 'Ferenc Sic',
                role: 'Frontend Developer',
                role_description: 'Ferenc creates EasyApply\'s engaging and intuitive user interface. He collaborates with the Product Mischief and UX designers to build user-friendly experiences for job seekers and recruiters. Passionate about web technologies and design trends, they ensure our platform stays visually appealing, accessible, and responsive across devices.',
                email: 'ferenc@easyapply.app',
                phone: null,
                linkedin: null,
                avatar: '/static/images/team/joao.png',
            },
            {
                name: 'Johnny Cartucho',
                role: 'Business Advisor',
                role_description: 'Johnny brings a wealth of experience to EasyApply. His expertise in market trends, business development, and financial management helps guide our strategic direction and growth. Johnny\'s tailored advice and support play a crucial role in our ongoing success and impact in the job search market.',
                email: 'johnny@easyapply.app',
                phone: null,
                linkedin: null,
                avatar: '/static/images/team/joao.png',
            },
            {
                name: 'Dimitrij Djordjevic',
                role: 'Mystical Visual Artist',
                role_description: 'Dimitrij, our Mystical Visual Architect, brings magic to EasyApply with his captivating designs and creative direction. He skillfully transforms ideas into enchanting visuals, ensuring a cohesive and memorable brand identity across all platforms. Dimitrij\'s artistic talent and keen understanding of user experience set EasyApply apart from the competition.',
                email: 'dimitrij@easyapply.app',
                phone: null,
                linkedin: null,
                avatar: '/static/images/team/joao.png',
            },

        ]
    });

    return { EasyApplyTeamData }

}