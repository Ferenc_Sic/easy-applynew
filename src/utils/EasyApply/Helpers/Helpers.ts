import { ref, computed } from 'vue';

export default function Helpers() {

    const FormatCurrency = (amount: number, currency: string) => {

        return new Intl.NumberFormat(navigator.language, {
            style: 'currency',
            currency: currency
        }).format(amount);

    };

    const VisitRouteByName = (context: any, name: string, params = {}, hash = '') => {
        context.$router.push({
            name: name,
            params: params,
            hash: hash,
        })
    };

    return { FormatCurrency, VisitRouteByName }

}