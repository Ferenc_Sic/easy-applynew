import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
import Homepage from '../app/views/Homepage.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/:market?/:country?/:language?',
        name: 'Homepage',
        component: Homepage,
        meta: { middleware: [ 'public' ] },
    },

    {
        path: '/:market?/:country?/:language?/about-us',
        name: 'AboutUs',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/EasyApply/AboutUs.vue'),
        meta: { middleware: [ 'public' ] },
    },
    {
        path: '/:market?/:country?/:language?/personality-types',
        name: 'PersonalityTypes',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/EasyApply/PersonalityTypes.vue'),
        meta: { middleware: [ 'public' ] },
    },


    // Authentication related routes:
    {
        path: '/:market?/:country?/:language?/account/:role/sign-in',
        name: 'SignIn',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/EasyApply/Account/Authentication/SignIn.vue'),
        meta: { middleware: [ 'public' ] },
    },
    {
        path: '/:market?/:country?/:language?/account/:role/create-account',
        name: 'CreateAccount',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/EasyApply/Account/Authentication/Registration.vue'),
        meta: { middleware: [ 'public' ] },
    },

    /**
     * Job Seekers Section:
     */
    {
        path: '/:market?/:country?/:language?/job-seekers',
        name: 'JobSeekers',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/JobSeekers/index.vue'),
        meta: { middleware: [ 'public' ] },
    },
    {
        path: '/:market?/:country?/:language?/job-seekers/job/search',
        name: 'JobSeekersJobSearch',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/JobSeekers/Job/Search.vue'),
        meta: { middleware: [ 'public' ] },
    },

    /**
     * Employers Section
     */
    {
        path: '/:market?/:country?/:language?/employers',
        name: 'Employers',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'public' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/dashboard',
        name: 'EmployersDashboard',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/jobs',
        name: 'EmployersJobs',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/jobs/list',
        name: 'EmployersJobsList',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/talent',
        name: 'EmployersTalent',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/companies',
        name: 'EmployersCompanies',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/companies/new',
        name: 'EmployersCompaniesNew',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/services',
        name: 'EmployersServices',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },
    {
        path: '/:market?/:country?/:language?/employers/services/curriculum-vitae-structure',
        name: 'EmployersServicesCurriculumVitaeStructure',
        component: () => import(/* webpackChunkName: "jobs" */ '../app/views/Employers/index.vue'),
        meta: { middleware: [ 'private', 'employer' ] },
    },


]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
